﻿namespace Renamer
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPrzegladaj = new System.Windows.Forms.Button();
            this.tbPath = new System.Windows.Forms.TextBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.btnZmien = new System.Windows.Forms.Button();
            this.tbNazwa = new System.Windows.Forms.TextBox();
            this.tbSezonOd = new System.Windows.Forms.TextBox();
            this.tbDopisek = new System.Windows.Forms.TextBox();
            this.tbSezonDo = new System.Windows.Forms.TextBox();
            this.tbOdcinekOd = new System.Windows.Forms.TextBox();
            this.tbOdcinekDo = new System.Windows.Forms.TextBox();
            this.tbNowyFolder = new System.Windows.Forms.TextBox();
            this.lblFileName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnPrzegladaj
            // 
            this.btnPrzegladaj.Location = new System.Drawing.Point(348, 10);
            this.btnPrzegladaj.Name = "btnPrzegladaj";
            this.btnPrzegladaj.Size = new System.Drawing.Size(75, 23);
            this.btnPrzegladaj.TabIndex = 0;
            this.btnPrzegladaj.Text = "Przeglądaj";
            this.btnPrzegladaj.UseVisualStyleBackColor = true;
            this.btnPrzegladaj.Click += new System.EventHandler(this.BtnFind_Click);
            // 
            // tbPath
            // 
            this.tbPath.Location = new System.Drawing.Point(12, 12);
            this.tbPath.Name = "tbPath";
            this.tbPath.ReadOnly = true;
            this.tbPath.Size = new System.Drawing.Size(330, 20);
            this.tbPath.TabIndex = 1;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(12, 93);
            this.listBox1.Name = "listBox1";
            this.listBox1.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBox1.Size = new System.Drawing.Size(461, 329);
            this.listBox1.TabIndex = 2;
            // 
            // btnZmien
            // 
            this.btnZmien.Location = new System.Drawing.Point(429, 36);
            this.btnZmien.Name = "btnZmien";
            this.btnZmien.Size = new System.Drawing.Size(48, 23);
            this.btnZmien.TabIndex = 3;
            this.btnZmien.Text = "Zmień";
            this.btnZmien.UseVisualStyleBackColor = true;
            this.btnZmien.Click += new System.EventHandler(this.btnZmien_Click);
            // 
            // tbNazwa
            // 
            this.tbNazwa.Location = new System.Drawing.Point(12, 39);
            this.tbNazwa.Name = "tbNazwa";
            this.tbNazwa.Size = new System.Drawing.Size(139, 20);
            this.tbNazwa.TabIndex = 4;
            this.tbNazwa.Text = "Nowa Nazwa";
            this.tbNazwa.TextChanged += new System.EventHandler(this.tbNazwa_TextChanged);
            // 
            // tbSezonOd
            // 
            this.tbSezonOd.Location = new System.Drawing.Point(157, 39);
            this.tbSezonOd.Name = "tbSezonOd";
            this.tbSezonOd.Size = new System.Drawing.Size(36, 20);
            this.tbSezonOd.TabIndex = 4;
            this.tbSezonOd.Text = "1";
            this.tbSezonOd.TextChanged += new System.EventHandler(this.tbNazwa_TextChanged);
            // 
            // tbDopisek
            // 
            this.tbDopisek.Location = new System.Drawing.Point(323, 39);
            this.tbDopisek.Name = "tbDopisek";
            this.tbDopisek.Size = new System.Drawing.Size(100, 20);
            this.tbDopisek.TabIndex = 5;
            this.tbDopisek.Text = "Dopisek";
            this.tbDopisek.TextChanged += new System.EventHandler(this.tbNazwa_TextChanged);
            // 
            // tbSezonDo
            // 
            this.tbSezonDo.Location = new System.Drawing.Point(199, 39);
            this.tbSezonDo.Name = "tbSezonDo";
            this.tbSezonDo.Size = new System.Drawing.Size(36, 20);
            this.tbSezonDo.TabIndex = 4;
            this.tbSezonDo.Text = "2";
            this.tbSezonDo.TextChanged += new System.EventHandler(this.tbNazwa_TextChanged);
            // 
            // tbOdcinekOd
            // 
            this.tbOdcinekOd.Location = new System.Drawing.Point(241, 39);
            this.tbOdcinekOd.Name = "tbOdcinekOd";
            this.tbOdcinekOd.Size = new System.Drawing.Size(36, 20);
            this.tbOdcinekOd.TabIndex = 4;
            this.tbOdcinekOd.Text = "1";
            this.tbOdcinekOd.TextChanged += new System.EventHandler(this.tbNazwa_TextChanged);
            // 
            // tbOdcinekDo
            // 
            this.tbOdcinekDo.Location = new System.Drawing.Point(283, 39);
            this.tbOdcinekDo.Name = "tbOdcinekDo";
            this.tbOdcinekDo.Size = new System.Drawing.Size(36, 20);
            this.tbOdcinekDo.TabIndex = 4;
            this.tbOdcinekDo.Text = "12";
            this.tbOdcinekDo.TextChanged += new System.EventHandler(this.tbNazwa_TextChanged);
            // 
            // tbNowyFolder
            // 
            this.tbNowyFolder.Location = new System.Drawing.Point(12, 67);
            this.tbNowyFolder.Name = "tbNowyFolder";
            this.tbNowyFolder.Size = new System.Drawing.Size(139, 20);
            this.tbNowyFolder.TabIndex = 7;
            this.tbNowyFolder.Text = "Nowy Folder";
            // 
            // lblFileName
            // 
            this.lblFileName.AutoSize = true;
            this.lblFileName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblFileName.Location = new System.Drawing.Point(157, 67);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(186, 17);
            this.lblFileName.TabIndex = 8;
            this.lblFileName.Text = "Nowa Nazwa.S1.E1.Dopisek";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(486, 439);
            this.Controls.Add(this.lblFileName);
            this.Controls.Add(this.tbNowyFolder);
            this.Controls.Add(this.tbDopisek);
            this.Controls.Add(this.tbOdcinekDo);
            this.Controls.Add(this.tbOdcinekOd);
            this.Controls.Add(this.tbSezonDo);
            this.Controls.Add(this.tbSezonOd);
            this.Controls.Add(this.tbNazwa);
            this.Controls.Add(this.btnZmien);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.tbPath);
            this.Controls.Add(this.btnPrzegladaj);
            this.Name = "Form1";
            this.Text = "Renamer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnPrzegladaj;
        private System.Windows.Forms.TextBox tbPath;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button btnZmien;
        private System.Windows.Forms.TextBox tbNazwa;
        private System.Windows.Forms.TextBox tbSezonOd;
        private System.Windows.Forms.TextBox tbDopisek;
        private System.Windows.Forms.TextBox tbSezonDo;
        private System.Windows.Forms.TextBox tbOdcinekOd;
        private System.Windows.Forms.TextBox tbOdcinekDo;
        private System.Windows.Forms.TextBox tbNowyFolder;
        private System.Windows.Forms.Label lblFileName;
    }
}

