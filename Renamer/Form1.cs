﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Renamer
{
    public partial class Form1 : Form
    {
        string[] files;
        string filePath;

        public Form1()
        {
            InitializeComponent();
        }

        private void BtnFind_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fdb = new FolderBrowserDialog();
            if (fdb.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                tbPath.Text = fdb.SelectedPath;
                filePath = fdb.SelectedPath;
                files = null;
                files = Directory.GetFiles(fdb.SelectedPath);
                listBox1.Items.Clear();
                foreach (string file in files)
                {
                    if (Path.GetFileNameWithoutExtension(file) != "Thumbs")
                    {
                        listBox1.Items.Add(Path.GetFileNameWithoutExtension(file));
                    }
                }
            }
        }

        private void btnZmien_Click(object sender, EventArgs e)
        {
            try
            {
                if (tbNazwa.Text != "")
                {
                    int odcinekOd = Convert.ToInt32(tbOdcinekOd.Text);
                    int odcinekDo = Convert.ToInt32(tbOdcinekDo.Text);
                    int sezonOd = Convert.ToInt32(tbSezonOd.Text);
                    int sezonDo = Convert.ToInt32(tbSezonDo.Text);
                    //Get newFile Directory
                    string folderName = "\\" + tbNowyFolder.Text + "\\";
                    if (!Directory.Exists(filePath + folderName)) //If does not exist - create newFile Directory
                        Directory.CreateDirectory(filePath + folderName);

                    foreach (string file in files)
                    {
                        if (sezonOd > sezonDo)
                        {
                            break;
                        }
                        if (odcinekOd > odcinekDo)
                        {
                            odcinekOd = 1;
                            sezonOd++;
                        }

                        string[] tempFile = file.Split('.');
                        string fileExtension = tempFile[tempFile.Length - 1];

                        if (Path.GetFileNameWithoutExtension(file) != "Thumbs")
                        { //filePath: C:\\folder\\folder-z-Plikami
                            string newfile = filePath + folderName + tbNazwa.Text;
                            if (Convert.ToString(sezonOd) != "" && Convert.ToString(sezonDo) != "")
                                newfile += ".S" + Convert.ToString(sezonOd);
                            if (Convert.ToString(odcinekOd) != "" && Convert.ToString(odcinekDo) != "")
                                newfile += ".E" + Convert.ToString(odcinekOd++);
                            if (tbDopisek.Text != "")
                                newfile += "." + tbDopisek.Text;
                            newfile += "." + fileExtension;
                            File.Move(file, newfile);
                        }
                    }
                }
                else
                    MessageBox.Show("Nie podano nazwy pliku !");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void tbNazwa_TextChanged(object sender, EventArgs e)
        {
            string nowaNazwa="";
            if (tbNazwa.Text != "")
                nowaNazwa += tbNazwa.Text;
            if (tbSezonOd.Text != "" && tbSezonDo.Text!="")
                nowaNazwa += ".S" + tbSezonOd.Text;
            if (tbOdcinekOd.Text != "" && tbOdcinekDo.Text != "")
                nowaNazwa += ".E" + tbOdcinekOd.Text;
            if (tbDopisek.Text != "")
                nowaNazwa += "." + tbDopisek.Text;
            lblFileName.Text = nowaNazwa;
        }
    }
}
